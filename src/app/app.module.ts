import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RebelConNavModule } from 'rebel-con-nav/rebel-con-nav.module';
import { RouterModule } from '@angular/router';
import { StoreModule} from '@ngrx/store';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule,
    RebelConNavModule,
    StoreModule.forRoot({}),
    RouterModule.forRoot([])],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
